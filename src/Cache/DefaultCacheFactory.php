<?php

namespace Mgo\HttpCacheBundle\Cache;

use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class DefaultCacheFactory implements CacheFactoryInterface
{
    public function buildCache(): AbstractAdapter
    {
        return new FilesystemAdapter();
    }
}
