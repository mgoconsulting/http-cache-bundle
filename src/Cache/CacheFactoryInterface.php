<?php

namespace Mgo\HttpCacheBundle\Cache;

use Symfony\Component\Cache\Adapter\AbstractAdapter;

interface CacheFactoryInterface
{
    public function buildCache(): AbstractAdapter;
}
