<?php

namespace Mgo\HttpCacheBundle\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CacheHandler extends AbstractHandler
{
    const CACHE_ATTRIBUTE_NAME = '_httpcached';

    /* @var string|null */
    private $key = null;

    public function getCache(Request $request): ?string
    {
        if ($this->config['enabled']) {
            if ($this->key = $this->getCacheKey($request)) {
                $cache = $this->buildCache();
                if ($cache->hasItem($this->key)) {
                    $cached = $cache->getItem($this->key)->get();
                    $request->attributes->set(self::CACHE_ATTRIBUTE_NAME, true);
                    $routeName = $request->attributes->get('_route');
                    $this->logger->debug(
                        "cache found '{$this->key}' from route '{$routeName}'",
                        ['key' => $this->key, 'route' => $routeName]
                    );

                    return $cached['content'] ?? null;
                }
            }
        }

        return null;
    }

    public function setCache(Request $request, Response $response): void
    {
        // expects method getCache() has been called before
        if ($this->config['enabled']
            && !$request->attributes->get(self::CACHE_ATTRIBUTE_NAME)
            && $this->key
        ) {
            $cache = $this->buildCache();
            $cacheItem = $cache->getItem($this->key);
            $cacheItem->set([
                'key' => $this->key,
                'content' => $response->getContent(),
                'request' => $this->requestToArray($request),
            ]);
            $cache->save($cacheItem);
            $routeName = $request->attributes->get('_route');
            $this->logger->debug(
                "cache saved '{$this->key}' from route '{$routeName}'",
                ['key' => $this->key, 'route' => $routeName]
            );
        }
        $this->key = null;
    }

    private function getInvalidateConfig(string $routeName): ?array
    {
        if (isset($this->config['definitions'][$routeName]['invalidations'])) {
            return $this->config['definitions'][$routeName]['invalidations'];
        }

        return null;
    }

    private function getCacheKey(Request $request): ?string
    {
        $routeName = $request->attributes->get('_route');
        if (isset($this->config['definitions'][$routeName])) {
            $conf = $this->config['definitions'][$routeName];
            $elValues = [
                'route' => $routeName,
                'request' => $request,
            ];

            if ($conf['enabled'] // if route cache is enabled
                && $conf['cache']['key'] // if route cache has key
                && ( // condition
                    !$conf['cache']['condition']
                    || $this->expressionLanguage->evaluate($conf['cache']['condition'], $elValues)
                )
            ) {
                return (string) $this->expressionLanguage->evaluate($conf['cache']['key'], $elValues);
            }
        }

        return false;
    }

    private function requestToArray(Request $request): array
    {
        foreach ((array) $request as $key => $value) {
            $key = trim($key, chr(0).chr(42));
            if ($value instanceof \IteratorAggregate) {
                switch ($key) {
                    case 'attributes':
                        $value = [
                            'controller' => $value->get('_controller'),
                            'route' => $value->get('_route'),
                            'route_params' => $value->get('_route_params'),
                        ];
                        break;
                    case 'files':
                    case 'server':
                    case 'session':
                    case 'cookies':
                        continue 2;
                        break;
                    default:
                        $value = $value->all();
                        break;
                }
            }
            $array[$key] = $value;
        }

        return $array;
    }
}
