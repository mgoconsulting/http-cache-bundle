<?php

namespace Mgo\HttpCacheBundle\Handler;

use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractHandler
{
    use ContainerAwareTrait;

    /* @var \Symfony\Component\ExpressionLanguage\ExpressionLanguage */
    protected $expressionLanguage;

    /* @var \Psr\Log\LoggerInterface */
    protected $logger;

    /* @var array */
    protected $config;

    public function __construct(ContainerInterface $container)
    {
        $this->expressionLanguage = $container->get('mgo_http_cache.expression_language');
        $this->logger = $container->get('monolog.logger.httpcache');
        $this->config = $container->getParameter('mgo_http_cache.config');
        $this->setContainer($container);
    }

    protected function buildCache(): AbstractAdapter
    {
        return $this->container->get('mgo_http_cache.cache_factory')->buildCache();
    }
}
