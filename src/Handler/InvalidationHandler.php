<?php

namespace Mgo\HttpCacheBundle\Handler;

use Doctrine\Common\Util\ClassUtils;
use Symfony\Component\HttpFoundation\Request;

class InvalidationHandler extends AbstractHandler
{
    /* @var array */
    private static $keys = [];

    public function invalidateRoutes(Request $request): void
    {
        if ($this->config['enabled'] && ($routeName = $request->attributes->get('_route'))) {
            foreach ($this->config['definitions'] as $route => $conf) {
                $elValues = [
                    'route' => $route,
                    'request' => $request,
                    'container' => $this->container,
                ];
                if (isset($conf['invalidations']['routes'][$routeName])) {
                    $invalidation = $conf['invalidations']['routes'][$routeName];
                    if (!$invalidation['condition']
                        || $this->expressionLanguage->evaluate($invalidation['condition'], $elValues)
                    ) {
                        $key = (string) $this->expressionLanguage->evaluate(
                            $invalidation['key'],
                            $elValues
                        );
                        $cache = $this->buildCache();
                        if ($key && $cache->hasItem($key)) {
                            $cache->deleteItem($key);
                            $this->logger->debug(
                                "invalidation route '{$route}' from '{$routeName}'",
                                ['key' => $key]
                            );
                        }
                    }
                }
            }
        }
    }

    public function invalidateEntity($entity): void
    {
        if ($this->config['enabled'] && is_object($entity)) {
            foreach ($this->config['definitions'] as $route => $conf) {
                $entityClass = ClassUtils::getRealClass(get_class($entity));
                $elValues = [
                    'route' => $route,
                    'entity' => $entity,
                    'entity_class' => $entityClass,
                    'container' => $this->container,
                ];
                if (isset($conf['invalidations']['orm'][$entityClass])) {
                    $invalidation = $conf['invalidations']['orm'][$entityClass];
                    if (!$invalidation['condition']
                        || $this->expressionLanguage->evaluate($invalidation['condition'], $elValues)
                    ) {
                        $key = (string) $this->expressionLanguage->evaluate(
                            $invalidation['key'],
                            $elValues
                        );
                        $cache = $this->buildCache();
                        if ($key && $cache->hasItem($key)) {
                            $cache->deleteItem($key);
                            $this->logger->debug(
                                "invalidation entity '{$entityClass}'",
                                ['key' => $key, 'route' => $route, 'entity_class' => $entityClass]
                            );
                        }
                    }
                }
            }
        }
    }

    public function invalidateCustomized(Request $request): void
    {
        if ($invalidations = $this->getInvalidationConfig($request, 'customized')) {
            $routeName = $request->attributes->get('_route');
            $elValues = [
                'route' => $routeName,
                'request' => $request,
                'container' => $this->container,
            ];
            foreach ($invalidations as $invalidation) {
                if (!$invalidation['condition']
                    || $this->expressionLanguage->evaluate($invalidation['condition'], $elValues)
                ) {
                    $key = (string) $this->expressionLanguage->evaluate(
                        $invalidation['key'],
                        $elValues
                    );
                    $cache = $this->buildCache();
                    if ($key && $cache->hasItem($key)) {
                        $cache->deleteItem($key);
                        $this->logger->debug(
                            "invalidation customized from route '{$routeName}'",
                            ['key' => $key, 'route' => $routeName]
                        );
                    }
                }
            }
        }
    }

    private function getInvalidationConfig(Request $request, string $type): ?array
    {
        if ($this->config['enabled']
            && ($routeName = $request->attributes->get('_route'))
            && isset($this->config['definitions'][$routeName]['invalidations'][$type])
        ) {
            return $this->config['definitions'][$routeName]['invalidations'][$type];
        }

        return null;
    }
}
