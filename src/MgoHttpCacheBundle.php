<?php

namespace Mgo\HttpCacheBundle;

use Mgo\HttpCacheBundle\DependencyInjection\Compiler\CacheFactoryPass;
use Mgo\HttpCacheBundle\DependencyInjection\MgoHttpCacheExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Config Bundle.
 */
class MgoHttpCacheBundle extends Bundle
{
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = $this->createContainerExtension();
        }

        return $this->extension;
    }

    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new CacheFactoryPass());
    }

    protected function getContainerExtensionClass()
    {
        return MgoHttpCacheExtension::class;
    }
}
