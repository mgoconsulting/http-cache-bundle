<?php

namespace Mgo\HttpCacheBundle\DependencyInjection\Compiler;

use Mgo\HttpCacheBundle\Cache\DefaultCacheFactory;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class CacheFactoryPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('mgo_http_cache.cache_factory')) {
            $definition = new Definition(DefaultCacheFactory::class);
            $definition->setPublic(true);
            $container->setDefinition('mgo_http_cache.cache_factory', $definition);
        }
    }
}
