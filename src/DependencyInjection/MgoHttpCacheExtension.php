<?php

namespace Mgo\HttpCacheBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Mgo Config Extension.
 */
class MgoHttpCacheExtension extends Extension implements PrependExtensionInterface
{
    const ALIAS = 'mgo_http_cache';

    public function getAlias()
    {
        return self::ALIAS;
    }

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        // load yaml files
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('suscribers.yml');
        // set config in parameters
        $container->setParameter(self::ALIAS.'.config', $config);
    }

    public function prepend(ContainerBuilder $container)
    {
        $container->prependExtensionConfig(
            'monolog',
            [
                'channels' => ['httpcache'],
            ]
        );
    }
}
