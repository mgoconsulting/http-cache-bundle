<?php

namespace Mgo\HttpCacheBundle\DependencyInjection;

use Doctrine\ORM\Events;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder(MgoHttpCacheExtension::ALIAS);
        // config component version
        if (\method_exists($treeBuilder, 'getRootNode')) {
            $root = $treeBuilder->getRootNode();
        } else {
            $root = $treeBuilder->root(MgoHttpCacheExtension::ALIAS);
        }

        /* @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $root */
        $root
            ->canBeDisabled()
            ->children()
                ->arrayNode('options')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('throw_exception')
                            ->treatNullLike([])
                            ->defaultValue(['dev', 'test'])
                            ->scalarPrototype()->end()
                        ->end()
                        ->arrayNode('orm')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->arrayNode('events')
                                    ->defaultValue($this->getORMEvents())
                                    ->scalarPrototype()
                                        ->validate()
                                            ->ifNotInArray($this->getORMEvents())
                                            ->thenInvalid('Invalid event %s')
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('definitions')
                    ->useAttributeAsKey('route')
                    ->arrayPrototype()
                        ->canBeDisabled()
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->arrayNode('cache')
                                ->addDefaultsIfNotSet()
                                ->treatNullLike(['key' => null])
                                ->beforeNormalization()
                                    ->ifString()
                                    ->then(function ($key) {
                                        return ['key' => $key];
                                    })
                                ->end()
                                ->children()
                                    ->scalarNode('condition')
                                        ->defaultNull()
                                    ->end()
                                    ->scalarNode('key')
                                        // default route name as key
                                        ->defaultValue('route')
                                    ->end()
                                ->end()
                            ->end()
                            ->arrayNode('invalidations')
                                ->children()
                                    ->arrayNode('routes')
                                        ->useAttributeAsKey('route')
                                        ->arrayPrototype()
                                            ->addDefaultsIfNotSet()
                                            ->treatNullLike(['key' => null])
                                            ->beforeNormalization()
                                                ->ifString()
                                                ->then(function ($key) {
                                                    return ['key' => $key];
                                                })
                                            ->end()
                                            ->children()
                                                ->scalarNode('condition')
                                                    ->defaultNull()
                                                ->end()
                                                ->scalarNode('key')
                                                    // default route name as key
                                                    ->defaultValue('route')
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                    ->arrayNode('orm')
                                        ->useAttributeAsKey('entity')
                                        ->arrayPrototype()
                                            ->addDefaultsIfNotSet()
                                            ->treatNullLike(['key' => null])
                                            ->beforeNormalization()
                                                ->ifString()
                                                ->then(function ($key) {
                                                    return ['key' => $key];
                                                })
                                            ->end()
                                            ->children()
                                                ->scalarNode('condition')
                                                    ->defaultNull()
                                                ->end()
                                                ->scalarNode('key')
                                                    ->isRequired()
                                                    ->cannotBeEmpty()
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                    ->arrayNode('customized')
                                        ->arrayPrototype()
                                            ->addDefaultsIfNotSet()
                                            ->treatNullLike(['key' => null])
                                            ->beforeNormalization()
                                                ->ifString()
                                                ->then(function ($key) {
                                                    return ['key' => $key];
                                                })
                                            ->end()
                                            ->children()
                                                ->scalarNode('condition')
                                                    ->defaultNull()
                                                ->end()
                                                ->scalarNode('key')
                                                    ->isRequired()
                                                    ->cannotBeEmpty()
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

    private function getORMEvents()
    {
        $reflectionClass = new \ReflectionClass(Events::class);
        $events = $reflectionClass->getConstants();

        // remove non-lifecycle events.
        unset(
            $events[Events::loadClassMetadata],
            $events[Events::onClassMetadataNotFound],
            $events[Events::preFlush],
            $events[Events::onFlush],
            $events[Events::postFlush],
            $events[Events::onClear]
        );

        return array_values($events);
    }
}
