<?php

namespace Mgo\HttpCacheBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Mgo\HttpCacheBundle\Handler\InvalidationHandler;

class DoctrineSubscriber implements EventSubscriber
{
    /* @var InvalidationHandler */
    private $invalidationHandler;

    /* @var array */
    private $config;

    public function __construct(InvalidationHandler $invalidationHandler, array $config)
    {
        $this->invalidationHandler = $invalidationHandler;
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return $this->config['options']['orm']['events'] ?? [];
    }

    public function __call(string $method, array $args)
    {
        if (in_array($method, $this->getSubscribedEvents()) && $args) {
            $this->invalidate($args[0]);
        }
    }

    private function invalidate(LifecycleEventArgs $event)
    {
        $this->invalidationHandler->invalidateEntity($event->getEntity());
    }
}
