<?php

namespace Mgo\HttpCacheBundle\EventSubscriber;

use Mgo\HttpCacheBundle\Handler\CacheHandler;
use Mgo\HttpCacheBundle\Handler\InvalidationHandler;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class KernelSubscriber implements EventSubscriberInterface
{
    const CACHE_ATTRIBUTE_NAME = '_httpcached';

    /* @var CacheHandler */
    private $cacheHandler;

    /* @var InvalidationHandler */
    private $invalidationHandler;

    /* @var LoggerInterface */
    private $logger;

    /* @var array */
    private $config;

    /* @var array */
    private $env;

    public function __construct(
        CacheHandler $cacheHandler,
        InvalidationHandler $invalidationHandler,
        LoggerInterface $logger,
        array $config,
        string $env
    ) {
        $this->cacheHandler = $cacheHandler;
        $this->invalidationHandler = $invalidationHandler;
        $this->logger = $logger;
        $this->config = $config;
        $this->env = $env;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onController',
            KernelEvents::TERMINATE => 'onTerminate',
        ];
    }

    public function onController(KernelEvent $event)
    {
        $request = $event->getRequest();

        try {
            // get cache if exists
            if ($content = $this->cacheHandler->getCache($request)) {
                $event->stopPropagation();
                $event->setController(function () use ($content) {
                    return new Response($content);
                });
            }
        } catch (\Exception $e) {
            if (in_array($this->env, $this->config['options']['throw_exception'])) {
                throw $e;
            }
            $this->logger->critical(
                $e->getMessage(),
                [
                    'action' => __FUNCTION__,
                    'route' => $request->attributes->get('_route'),
                ]
            );
        }
    }

    public function onTerminate(PostResponseEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();

        try {
            $this->cacheHandler->setCache($request, $response);
            $this->invalidationHandler->invalidateRoutes($request);
            $this->invalidationHandler->invalidateCustomized($request);
        } catch (\Exception $e) {
            if (in_array($this->env, $this->config['options']['throw_exception'])) {
                throw $e;
            }
            $this->logger->critical(
                $e->getMessage(),
                [
                    'action' => __FUNCTION__,
                    'route' => $request->attributes->get('_route'),
                ]
            );
        }
    }
}
