# **MGO HTTP Cache Bundle**


## **Installation**

#### Installation with composer
```sh
composer config repositories.mgoconsulting/http-cache-bundle git https://bitbucket.org/mgoconsulting/http-cache-bundle.git
composer require mgoconsulting/http-cache-bundle
```
## Enable the bundle

In *config/bundles.php*
```php
<?php
return [
    ...
    Mgo\HttpCacheBundle\MgoHttpCacheBundle::class => ['prod' => true],
];
```

## Configuration reference

In *config/packages/mgo_http_cache.yaml*
```yaml
mgo_http_cache:
    enabled: true # fully enable or disable the http cache bundle
    options:
        throw_exception: [dev, test] # throw exception for these envs
        orm:
            events: # list of doctrine events to listen to
                - preRemove
                - postRemove
                - prePersist
                - postPersist
                - preUpdate
                - postUpdate
                - postLoad
    definitions:
        # Prototype
        route:
            enabled: true # enable or disable this http cache definition
            cache:
                condition: 'request.getMethod() == "GET"' # optional expression language condition
                key: 'ROUTE_KEY' # string cache key
            invalidations: 
                routes: # invalidation by route
                    # Prototype
                    route_name:
                        condition: null
                        key: route
                orm: # invalidation by entity
                    # Prototype
                    App\Entity\Book:
                        condition: 'entity.id > 10' # optional expression language condition
                        key: 'entity_key_cache' # Required
                customized: # invalidation custom (access to container services)
                    cusom name:
                        condition:'container.get("my_service").getCache()' # can call a service to return a cache string or null
```

## Configuration examples

In *config/packages/mgo_http_cache.yaml*
```yaml
mgo_http_cache:
    definitions:
        # always cached
        route_name_1:
            cache: 'KEY_NAME_ROUTE_1'

        # always cached with different key by id
        route_name_2:
            cache:
                key: 'route ~ "_" ~ request.attributes.get("_route_params")["id"]'

        # always cached with condition, only POST
        route_name_3:
            cache:
                key: 'KEY_NAME_ROUTE_3'
                condition: 'request.getMethod() == "POST"'

        # always cached with invalidation by route name
        route_name_4:
            cache: 'KEY_NAME_ROUTE_4'
            invalidations:
                # invalidation by route name
                routes:
                    # When I visit route_name_1, I invalidate cache for route_name_1
                    route_name_1:
                        key: 'KEY_NAME_ROUTE_1'

        # always cached with invalidation by entity
        route_name_5:
            cache: 'KEY_NAME_ROUTE_5'
            invalidations:
                # invalidation by route name
                orm:
                    # When I persist a book entity, I invalidate cache for route_name_4
                    App\Entity\Book:
                        key: 'KEY_NAME_ROUTE_4'

        # always cached with custom invalidation
        route_name_6:
            cache: 'KEY_NAME_ROUTE_6'
            invalidations:
                # custom invalidation
                customized:
                    # When I call a service and a string is returned as key
                    App\Entity\Book:
                        condition: 'container.get("my_service").getCache(request)'
```


## Handlers

### CacheHandler
Handle the cache
`Mgo\HttpCacheBundle\Handler\CacheHandler`

### InvalidationHandler
Handle the invalidation
`Mgo\HttpCacheBundle\Handler\InvalidationHandler`
