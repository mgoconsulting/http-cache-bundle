<?php

namespace Test;

use Mgo\HttpCacheBundle\Cache\CacheFactoryInterface;
use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class CacheFactory implements CacheFactoryInterface
{
    public function buildCache(): AbstractAdapter
    {
        return new FilesystemAdapter('test', 30, dirname(__DIR__).'/var');
    }
}
