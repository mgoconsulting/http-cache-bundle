<?php

namespace Test;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\RouteCollectionBuilder;
use Test\Entity\Entity;

// phpcs:disable
require __DIR__.'/../vendor/autoload.php';
// phpcs:enable

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function registerBundles()
    {
        return [
            new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new \Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new \Symfony\Bundle\MonologBundle\MonologBundle(),
            new \Mgo\HttpCacheBundle\MgoHttpCacheBundle(),
        ];
    }

    public function responseAction()
    {
        return new Response(uniqid());
    }

    protected function configureRoutes(RouteCollectionBuilder $routes)
    {
        $routes->add('/not-cached', 'kernel::responseAction');
        $routes->add('/disabled', 'kernel::responseAction');
        $routes->add('/always-cached', 'kernel::responseAction');
        $routes->add('/cached-if-get-param', 'kernel::responseAction');
        $routes->add('/condition/{name}', 'kernel::responseAction');
        $routes->add('/get-and-post', 'kernel::responseAction');
    }

    protected function configureContainer(ContainerBuilder $builder, LoaderInterface $loader)
    {
        $dbPath = sys_get_temp_dir().'/mgo-http-cache-test.db';
        @unlink($dbPath);

        $builder->loadFromExtension('framework', [
            'secret' => 'TEST',
            'test' => true,
        ]);

        $builder->loadFromExtension('doctrine', [
            'dbal' => [
                'default_connection' => 'test',
                'connections' => [
                    'test' => [
                        'driver' => 'pdo_sqlite',
                        'path' => $dbPath,
                    ],
                ],
            ],
            'orm' => [
                'mappings' => [
                    'tests' => [
                        'type' => 'annotation',
                        'prefix' => 'Test\Entity',
                        'dir' => __DIR__.'/Entity',
                    ],
                ],
            ],
        ]);

        $builder->loadFromExtension('mgo_http_cache', [
            'definitions' => [
                '_always_cached' => [
                    'cache' => '"KEY_ALWAYS_CACHED"',
                    'invalidations' => [
                        'customized' => ['"CONDITION"'],
                        'orm' => [
                            Entity::class => '"KEY_ALWAYS_CACHED"',
                        ],
                    ],
                ],
                '_cached_if_get_param' => [
                    'cache' => [
                        'condition' => 'request.query.all()',
                        'key' => '"CACHED_GET_PARAM"',
                    ],
                    'invalidations' => [
                        'routes' => [
                            '_always_cached' => '"CACHED_GET_PARAM"',
                            '_condition_name' => [
                                'condition' => 'request.attributes.get("name") == "invalidate"',
                                'key' => '"CACHED_GET_PARAM"',
                            ],
                        ],
                    ],
                ],
                '_disabled' => [
                    'enabled' => false,
                    'cache' => '"DISABLED"',
                ],
                '_condition_name' => [
                    'cache' => [
                        'condition' => 'request.attributes.get("name") == "CACHED"',
                        'key' => '"CONDITION"',
                    ],
                ],
                '_get_and_post' => [
                    'cache' => '"GET_&_POST"',
                    'invalidations' => [
                        'customized' => [
                            ['key' => '"GET_&_POST"', 'condition' => 'request.getMethod() == "POST"'],
                        ],
                    ],
                ],
            ],
        ]);

        // override cache factory class to set FilesystemAdapter directory
        $builder->setParameter('mgo_http_cache.cache_factory_class', CacheFactory::class);
    }
}

// phpcs:disable
if ('cli' !== php_sapi_name()) {
    define('_CLI', 0);
    $kernel = new Kernel('test', true);
    $request = Request::createFromGlobals();
    $response = $kernel->handle($request);
    $response->send();
    $kernel->terminate($request, $response);
}
