<?php

namespace Test;

use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Test\Entity\Entity;

class CacheControllerTest extends WebTestCase
{
    const MAX_RAND = 10;

    /** @var \Symfony\Bundle\FrameworkBundle\KernelBrowser */
    private static $client;

    /** @var \Doctrine\Bundle\DoctrineBundle\Registry */
    private static $doctrine;

    /** @var \Doctrine\ORM\EntityManager */
    private static $em;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$client = self::createClient(['debug' => self::isDebug()]);
        // get doctrine services
        self::$doctrine = self::$kernel->getContainer()->get('doctrine');
        self::$em = self::$doctrine->getManagerForClass(Entity::class);
        // create db
        self::buildDb();
    }

    protected static function isDebug(): bool
    {
        return \in_array('--debug', $_SERVER['argv'] ?? []);
    }

    private static function buildDb(): void
    {
        $em = self::$doctrine->getManager();
        $connection = $em->getConnection();
        $database = $connection->getDatabase();

        $schemaManager = $connection->getSchemaManager();
        $schemaManager->dropDatabase($database);
        $schemaManager->createDatabase($database);

        $schemaTool = new SchemaTool($em);
        $schemaTool->createSchema($em->getMetadataFactory()->getAllMetadata());
    }

    public function testNotCached()
    {
        $crawler = self::$client->request('GET', '/not-cached');
        $content = $crawler->html();

        // response should be cached
        for ($x = 0; $x <= rand(1, self::MAX_RAND); ++$x) {
            $crawler = self::$client->request('GET', '/not-cached');
            $this->assertNotEquals($content, $crawler->html());
        }
    }

    public function testDisabled()
    {
        $crawler = self::$client->request('GET', '/disabled');
        $content = $crawler->html();

        // response should be cached
        for ($x = 0; $x <= rand(1, self::MAX_RAND); ++$x) {
            $crawler = self::$client->request('GET', '/disabled');
            $this->assertNotEquals($content, $crawler->html());
        }
    }

    public function testAlwaysCached()
    {
        $crawler = self::$client->request('GET', '/always-cached');
        $cached = $crawler->html();

        // response should be cached
        for ($x = 0; $x <= rand(1, self::MAX_RAND); ++$x) {
            $crawler = self::$client->request('GET', '/always-cached');
            $this->assertEquals($cached, $crawler->html());
        }
    }

    public function testCondition()
    {
        $crawler = self::$client->request('GET', '/condition/aaaa');
        $content = $crawler->html();
        // should not be cached
        for ($x = 0; $x <= rand(1, self::MAX_RAND); ++$x) {
            $crawler = self::$client->request('GET', '/condition/aaaa');
            $this->assertNotEquals($content, $crawler->html());
        }

        $crawler = self::$client->request('GET', '/condition/CACHED');
        $cached = $crawler->html();

        // should be cached
        for ($x = 0; $x <= rand(1, self::MAX_RAND); ++$x) {
            $crawler = self::$client->request('GET', '/condition/CACHED');
            $this->assertEquals($cached, $crawler->html());
        }
    }

    public function testConditionOnRequest()
    {
        $crawler = self::$client->request('GET', '/cached-if-get-param');
        $content = $crawler->html();
        // should not be cached
        for ($x = 0; $x <= rand(1, self::MAX_RAND); ++$x) {
            $crawler = self::$client->request('GET', '/cached-if-get-param');
            $this->assertNotEquals($content, $crawler->html());
        }

        $crawler = self::$client->request('GET', '/cached-if-get-param?get_param=1');
        $content = $crawler->html();
        // should be cached
        for ($x = 0; $x <= rand(1, self::MAX_RAND); ++$x) {
            $crawler = self::$client->request('GET', '/cached-if-get-param?get_param=1');
            $this->assertEquals($content, $crawler->html());
        }
    }

    public function testRoutesInvalidation()
    {
        $crawler = self::$client->request('GET', '/cached-if-get-param?get_param=1');
        $content = $crawler->html();

        // should be cached
        for ($x = 0; $x <= rand(1, self::MAX_RAND); ++$x) {
            $crawler = self::$client->request('GET', '/cached-if-get-param?get_param=1');
            $this->assertEquals($content, $cached = $crawler->html());
        }

        // call /always-cached to invalidate
        $crawler = self::$client->request('GET', '/always-cached');
        // cache should be cleared
        $crawler = self::$client->request('GET', '/cached-if-get-param?get_param=1');
        $this->assertNotEquals($cached, $crawler->html());
    }

    public function testRoutesInvalidationWithCondition()
    {
        $crawler = self::$client->request('GET', '/cached-if-get-param?get_param=1');
        $content = $crawler->html();
        // should be cached
        for ($x = 0; $x <= rand(1, self::MAX_RAND); ++$x) {
            $crawler = self::$client->request('GET', '/cached-if-get-param?get_param=1');
            $this->assertEquals($content, $cached = $crawler->html());
            break;
        }

        // call /condition/not-invalidate
        $crawler = self::$client->request('GET', '/condition/not-invalidate');
        // cache should NOT be cleared
        $crawler = self::$client->request('GET', '/cached-if-get-param?get_param=1');
        $this->assertEquals($cached, $crawler->html());

        // call /condition/invalidate to REALLY invalidates
        $crawler = self::$client->request('GET', '/condition/invalidate');
        // cache should be cleared
        $crawler = self::$client->request('GET', '/cached-if-get-param?get_param=1');
        $this->assertNotEquals($cached, $crawler->html());
    }

    public function testEntityInvalidation()
    {
        $crawler = self::$client->request('GET', '/always-cached');
        $cached = $crawler->html();

        // response should be cached
        for ($x = 0; $x <= rand(1, self::MAX_RAND); ++$x) {
            $crawler = self::$client->request('GET', '/always-cached');
            $this->assertEquals($cached, $crawler->html());
        }

        // persist entity should clear cache
        $entity = new Entity();
        self::$em->persist($entity);
        $crawler = self::$client->request('GET', '/always-cached');
        $this->assertNotEquals($cached, $crawler->html());

        self::$em->flush();
        $crawler = self::$client->request('GET', '/always-cached');
        $this->assertNotEquals($cached, $crawler->html());
    }

    public function testCustomizedInvalidation()
    {
        $crawler = self::$client->request('GET', '/get-and-post');
        $cached = $crawler->html();

        // should be cached
        for ($x = 0; $x <= rand(1, self::MAX_RAND); ++$x) {
            $crawler = self::$client->request('GET', '/get-and-post');
            $this->assertEquals($cached, $crawler->html());
        }

        // invalidate with POST
        $crawler = self::$client->request('POST', '/get-and-post');
        // should be cached from POST request
        $crawler = self::$client->request('GET', '/get-and-post');
        $this->assertNotEquals($cached, $crawler->html());
    }

    public function testCustomizedInvalidationWithCondition()
    {
        $crawler = self::$client->request('GET', '/condition/CACHED');
        $cached = $crawler->html();

        // should be cached
        for ($x = 0; $x <= rand(1, self::MAX_RAND); ++$x) {
            $crawler = self::$client->request('GET', '/condition/CACHED');
            $this->assertEquals($cached, $crawler->html());
        }

        // call /always-cached to invalidate /condition/CACHED
        $crawler = self::$client->request('GET', '/always-cached');
        // /condition/CACHED should be different
        $crawler = self::$client->request('GET', '/condition/CACHED');
        $this->assertNotEquals($cached, $crawler->html());
    }
}
